$(document).on('change', 'input[name="select_all"]', function(){
      var $this = $(this);
      var div = $this.parent();
      var ischecked = $this.prop('checked');
      div.find(':checkbox').not($this).prop('checked', ischecked);

});

$(document).on('change', 'input[name="select_all_together"]', function(){
      var $this = $(this);
      var div = $this.parents();
      var ischecked = $this.prop('checked');
      div.find(':checkbox').not($this).prop('checked', ischecked);

});

$(document).ready(function(){

  // === leftmenu navigation === //

  $('.submenu > a').click(function(e)
  {
    e.preventDefault();
    var submenu = $(this).siblings('ul');
    var li = $(this).parents('li');
    var submenus = $('#leftmenu .submenu > ul');
    var submenus_parents = $('#leftmenu .submenu');
    if(li.hasClass('open'))
    {
      if(($(window).width() > 768) || ($(window).width() < 479)) {
        submenu.slideUp();
      } else {
        submenu.fadeOut(250);
      }
      li.removeClass('open');
    } else
    {
      if(($(window).width() > 768) || ($(window).width() < 479)) {
        submenus.slideUp();
        submenu.slideDown();
      } else {
        submenus.fadeOut(250);
        submenu.fadeIn(250);
      }
      submenus_parents.removeClass('open');
      li.addClass('open');
    }
  });

  // === Tooltips === //
  $('.tip').tooltip();
  $('.tip-left').tooltip({ placement: 'left' });
  $('.tip-right').tooltip({ placement: 'right' });
  $('.tip-top').tooltip({ placement: 'top' });
  $('.tip-bottom').tooltip({ placement: 'bottom' });

  $('.selectContent').change(function(){
      var selected = $(this).find(':selected');
      $('.optionSelected').hide();
      $('.'+selected.val()).show();
  });

  $('.datepicker').datepicker({
        format: 'dd/mm/yyyy'
  });

  $(window).load(function(){
    $("ol.progtrckr").each(function(){
        $(this).attr("data-progtrckr-steps",
                     $(this).children("li").length);
    });
  })

  $('.info-viagem').each(function() {
     $(this).click(function(){
        $(this).next().toggle();
     })
  });


  function onlyNumbers(e){
      var key=(window.event)?event.keyCode:e.which;
      if((key>47)&&(key<58)) return true;
      else{
          if (key==8 || key==0) return true;
      else  return false;
      }
  }

});